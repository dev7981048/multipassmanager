# Multipass Manager - V3 | _Tutoriel d'installation & configuration_

## Création de la VM

Via un terminal, cherchez votre carte réseau avec la commande `ipconfig`, puis executez :

- `multipass set local.bridged-network="<Nom de la carte réseau>"`
- `multipass launch --name <Nom de la VM> --disk 8G --memory 2G --cpus 2 --bridged`
- `multipass set local.privileged-mounts=true`

## Configuration de config.json

```
"vm": "<Nom de la VM>"
"path": "<Chemin absolut jusqu'au dossier mnt (Windows)>"
```

Attention ❗ Veuillez rajouter un back-slash après ceux déjà existants au lieu d'un seul.

Par exemple : ```"C:\\Users\\matdev\\Desktop\\mnt"```

## Descriptions des fichiers

### V1 & V2 :

- _**remount.bat**_ pour reactiver le mount de la VM.
- _**run.bat**_ pour lancer la VM et ouvrir la page du serveur Apache dans le navigateur.
- _**stop.bat**_ bah t'as capté à quoi ca sert

### V3 :

- _**config.json**_ stocke les paramètres de l'application, facilitant ainsi la personnalisation tout en préservant l'intégrité du code source.
- _**MultipassManager.EXE**_ est l'application dédiée à la gestion de Multipass.